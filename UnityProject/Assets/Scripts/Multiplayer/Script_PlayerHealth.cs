﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using UnityEngine.UI;

public class Script_PlayerHealth : NetworkBehaviour
{
    int prevHealth;
    float health;
    [SyncVar]
    float syncHealth;
    // Use this for initialization
    void Start()
    {

        health = 100;
        CmdSerStart();
    }

    [Command]
   void CmdSerStart()
    {
        syncHealth = 100;
    }

    // Update is called once per frame
    void Update()
    {
        SynchHealth();
        if (health <= 0)
        {
            Debug.Log("Player died");
        }

        if (isLocalPlayer)
        {
            GameObject.Find("HealthBar").GetComponent<Slider>().value = health;
        }



    }



    [Client]
    public void Heal(int healAmount)
    {
        if (healAmount > 0)
        {
            Debug.Log("Requesting perm to heal by " + healAmount);
            CmdHeal(healAmount);
        }
    }

    [Command]
    //Heals player
    public void CmdHeal(int amount)
    {
        syncHealth += amount;
        Debug.Log("Player has been healed by " + amount);
        Debug.Log("New health is " + health);
        if (health > 0)
            GameObject.Find("HealthBar").GetComponent<Slider>().value = health;
        else
            GameObject.Find("HealthBar").GetComponent<Slider>().value = 0;
    }

    public void Damage(int amount)
    {
        if (amount > 0)
        {
            Debug.Log("Requesting perm to damage by " + amount);
            CmdDamage(amount);
        }
    }

    [Command]
    //Damages player
    public void CmdDamage(int amount)
    {
        syncHealth -= amount;
        Debug.Log("Player has been damaged by " + amount);
        Debug.Log("New health is " + health);
        if (health > 0)
            GameObject.Find("HealthBar").GetComponent<Slider>().value = health;
        else
            GameObject.Find("HealthBar").GetComponent<Slider>().value = 0;
    }

    public void SynchHealth()
    {
        health = syncHealth;
    }
}
